import { getToken } from "../requests/requests.js";
import successLogin from "./successLogin.js";

async function login() {
    const email = this.emailInput.value;
    const password = this.passInput.value;
    const { token, isSuccess } = await getToken(email, password)
    if (isSuccess) {
        localStorage.setItem('token', `${token}`)
        successLogin(localStorage.getItem('token'));
        return isSuccess;
    }
}

export default login;

