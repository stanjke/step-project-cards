import Modal from "./Modal.js";

class EditModal extends Modal {
    constructor(options, submitEditFn) {
        super();
        this.options = options;
        this.submitEditFn = submitEditFn;
    }

    setSubmitEditBtn = () => {
        this.submitEditBtn = document.getElementById('editModalBtn')
        this.submitEditBtn.addEventListener('click', () => {
            this.submitEditFn();
            this.closeModal();
        })
    }

    renderCommonFields() {
        this.modalContentWrap.classList.add('scrollable', 'mt-3', 'p-3')
        this.modalContentWrap.insertAdjacentHTML('beforeend', `
            <form id="editForm">
                <div >
                    <label for="editVisitPurpose" class="form-label">Visit purpose</label>
                    <input name="visitPurpose" type="text" value="${this.options.visitPurpose}" class="form-control" id="editVisitPurpose" aria-describedby="emailHelp">
                    <div id="editVisitPurposeHelp" class="form-text">lorem ipsum lorem ipsum lorem ipsum</div>
                </div>
                <div >
                    <label for="exampleInputPassword1" class="form-label">Full name</label>
                    <input name="fullName" value="${this.options.fullName}" type="text" class="form-control" id="exampleInputPassword1">
                </div>
                <div >
                    <label for="exampleSelect" class="form-label">Urgency</label>
                    <select name="urgency" class="form-select form-select-sm" id="exampleSelect">
                    <option selected="">${this.options.urgency}</option>
                    <option value="Regular">Regular</option>
                    <option value="High-priority">High-priority</option>
                    <option value="Emergency">Emergancy</option>
                    </select>
                </div>
                <div class="input-group mt-3">
                    <span class="input-group-text">Description</span>
                    <textarea name="visitDescription" class="form-control" aria-label="With textarea">${this.options.visitDescription}</textarea>
                </div>
                <div class="">
                    <label for="exampleSelect" class="form-label">Status</label>
                    <select name="status" class="form-select form-select-sm" id="exampleSelect">
                    <option disabled selected>Change status...</option>
                    <option value="open">Open</option>
                    <option value="close">Close</option>
                </select>
            </div>
            </form>
        `)
        this.modalButtonWrap.insertAdjacentHTML('beforeend', `
            <button id="editModalBtn" type="submit" class="btn btn-primary">Submit</button>
        `)
        this.editForm = document.querySelector('#editForm')
        this.setSubmitEditBtn();
    }

    editCardiologist() {
        this.renderCommonFields();
        this.editForm.insertAdjacentHTML('beforeend', `
            <div class="">
                <label for="exampleInputPassword1" class="form-label">Body mass index</label>
                <input name="BMI" value="${this.options.BMI}" type="text" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="">
                <label for="exampleInputPassword1" class="form-label">Age</label>
                <input name="age" value="${this.options.age}" type="text" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="">
                <label for="exampleInputPassword1" class="form-label">Disease history</label>
                <input name="diseaseHistory" value="${this.options.diseaseHistory}" type="text" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="">
                <label for="exampleInputPassword1" class="form-label">Pressure</label>
                <input name="pressure" value="${this.options.pressure}" type="text" class="form-control" id="exampleInputPassword1">
            </div>
        `)
    }

    editDentist() {
        this.renderCommonFields();
        this.editForm.insertAdjacentHTML('beforeend', `
            <div class="">
                <label for="editLastVisit" class="form-label">Last visit</label>
                <input name="lastVisit" value="${this.options.lastVisit}" type="date" class="form-control" id="editLastVisit" aria-describedby="editLastVisitHelp">
                <div id="editLastVisitHelp" class="form-text">lorem ipsum lorem ipsum lorem ipsum</div>
            </div>
        `)
    }

    editTherapist() {
        this.renderCommonFields();
        this.editForm.insertAdjacentHTML('beforeend', `
        <div class="">
            <label for="editAge" class="form-label">Last visit</label>
            <input name="age" type="text" value="${this.options.age}" class="form-control" id="editAge" aria-describedby="editAgeHelp">
            <div id="editAgeHelp" class="form-text">lorem ipsum lorem ipsum lorem ipsum</div>
        </div>
        `)
    }
    render() {
        super.render();
        switch (this.options.doctor) {
            case 'Cardiologist':
                return this.editCardiologist();
            case 'Therapist':
                return this.editTherapist();
            default:
                return this.editDentist();

        }
    }
}




export default EditModal;