import { renderUserCards } from "./components/renderUserCards.js";
import successLogin from "./components/successLogin.js";
import loginHandler from "./components/loginHandler.js";
import logoutHandler from "./components/logoutHandler.js";
import createVisitHandler from "./components/createVisitHandler.js";
import filter from "./components/filter.js";


successLogin(localStorage.getItem("token"), renderUserCards);
loginHandler();
logoutHandler();
createVisitHandler();
filter();

