import Visit from "./Visit.js";

class DentistVisit extends Visit {
  constructor() {
    super();
    this.visitWrap = document.createElement("div");
  }

  render() {
    this.visitWrap.insertAdjacentHTML(
      "afterbegin",
      `<form class="row g-3" id="formElem">
        <div class="col-md-4">
          <label for="validationServer01" class="form-label">Full Name</label>
          <input type="text" class="form-control" id="validationServer01" name='fullName' value="name" required>
          <div class="valid-feedback">
            Looks good!
          </div>
        </div>
        <div class="col-md-4">
          <label for="validationServer02" class="form-label">Visit purpose</label>
          <input type="text" class="form-control" id="validationServer02" name='visitPurpose' value="visit" required>
          <div class="valid-feedback">
            Looks good!
          </div>
        </div>
        <div class="col-md-3">
          <label for="validationServer03" class="form-label">Urgency</label>
          <select class="form-select" id="validationServer03" name='urgency' required>
            <option selected disabled value="">Choose...</option>
            <option>Regular</option>
            <option>High-priority</option>
            <option>Emergency</option>
          </select>
          <div class="invalid-feedback">
            Please select a valid state.
          </div>
        </div>
        <div class="col-md-5">
          <label for="validationServer06" class="form-label">Date of last visit</label>
          <input type="date" class="form-control" id="validationServer06" name='lastVisit'  required>
          <div class="valid-feedback">
            Looks good!
          </div>
        </div>
        <div class="col-md-6">
          <label for="validationServer04" class="form-label">Visit description</label>
          <div class="input-group">
            <textarea class="form-control" aria-label="With textarea" value="fafaf" name='visitDescription'></textarea>
          </div>
        </div>
        <div class="col-12">
          <button class="btn btn-primary" type="submit">Submit form</button>
        </div>
    </form>`
    );
    return this.visitWrap;
  }
}

export default DentistVisit;
