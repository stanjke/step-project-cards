import Modal from "./Modal.js";

class SigninModal extends Modal {
    constructor(submitFn, showCardsFn) {
        super();
        this.submitFn = submitFn;
        this.showCardsFn = showCardsFn;
        this.form = document.createElement('form');
        this.formImg = document.createElement('img');
        this.formTitle = document.createElement('h1');
        this.formFloatingEmail = document.createElement('div');
        this.formFloatingPass = document.createElement('div');
        this.emailInput = document.createElement('input');
        this.emailLabel = document.createElement('label');
        this.passInput = document.createElement('input');
        this.passLabel = document.createElement('label');
        this.signinBtn = document.createElement('button');
    }

    createEl() {
        super.createEl();
        this.modalContentWrap.className = 'form-signin w-100 m-auto';
        this.modalContainer.classList.add('modal__main-container--small')
        this.formImg.classList.add('mb-4');
        this.formImg.setAttribute('src', './src/assets/free-icon-healthcare-2382461.png')
        this.formImg.setAttribute('width', '50')
        this.formImg.setAttribute('height', '50')
        this.formTitle.classList.add('h3', 'mb-3', 'fw-normal');
        this.formTitle.textContent = 'Please sign in'
        this.formFloatingEmail.classList.add('form-floating')
        this.formFloatingPass.classList.add('form-floating')
        this.emailInput.classList.add('form-control');
        this.emailInput.setAttribute('type', 'email');
        this.emailInput.setAttribute('id', 'floatingInput');
        this.emailInput.setAttribute('placeholder', 'name@example.com');
        this.emailLabel.setAttribute('for', 'floatingInput');
        this.emailLabel.textContent = 'Email address';
        this.passInput.classList.add('form-control');
        this.passInput.setAttribute('type', 'password');
        this.passInput.setAttribute('id', 'floatingPassword');
        this.passInput.setAttribute('placeholder', 'Password');
        this.passLabel.setAttribute('for', 'floatingPassword');
        this.passLabel.textContent = 'Password'
        this.signinBtn.classList.add('btn', 'btn-primary', 'w-100', 'py-2');
        this.signinBtn.setAttribute('type', 'submit');
        this.signinBtn.textContent = 'Sign in'

        this.formFloatingEmail.append(this.emailInput, this.emailLabel);
        this.formFloatingPass.append(this.passInput, this.passLabel);

        this.modalContentWrap.append(
            this.formImg,
            this.formTitle,
            this.formFloatingEmail,
            this.formFloatingPass,
            this.signinBtn
        )

        this.signinBtn.addEventListener('click', async event => {
            const isLogin = await this.submitFn();
            if (isLogin) {
                this.closeModal();
                this.showCardsFn();
            }

        })

    }

    showValues() {

    }
}

export default SigninModal;