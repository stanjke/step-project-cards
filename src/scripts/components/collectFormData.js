function collectFormData() {
  const form = document.querySelector('#editForm');
  const formData = new FormData(form);
  const formDataObject = {};
  formDataObject.doctor = this.options.doctor;
  formDataObject.id = this.options.id;

  for (const [key, value] of formData.entries()) {
    formDataObject[key] = value;
  }

  if (!formDataObject.hasOwnProperty('status')) {
    formDataObject.status = this.options.status
  }

  return formDataObject;
}

export default collectFormData;
