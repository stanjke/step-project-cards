export async function sendRequest(url, token, method = "GET", options) {
  const response = await fetch(url, {
    method: method,
    headers:
      method === "DELETE"
        ? {
            Authorization: `Bearer ${token}`,
          }
        : {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
    ...options,
  });
  if (method === "DELETE") {
    return response;
  } else {
    return response.json();
  }
}
