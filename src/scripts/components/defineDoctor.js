import CardiologistVisit from "./Visit/CardiologistVisit.js";
import DentistVisit from "./Visit/DentistVisit.js";
import TherapistVisit from "./Visit/TherapistVisit.js";

function defineDoctor(doctor) {
  switch (doctor) {
    case "Cardiologist":
      return new CardiologistVisit().render();
    case "Dentist":
      return new DentistVisit().render();
    case "Therapist":
      return new TherapistVisit().render();
  }
}

export default defineDoctor;
