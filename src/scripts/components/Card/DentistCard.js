import Card from "./Card.js";

class DentistCard extends Card {
  constructor(options, deleteFn, editFn) {
    super(options, deleteFn, editFn);
    }

    edit(object){
    super.edit(object)
  }

    showLessInfo = () => {
        this.showLessBtn = document.getElementById(`${this.options.id}`).querySelector('#showLess');
        this.showLessBtn.addEventListener('click', () => {
          this.addInfoWrap = document.getElementById(`${this.options.id}`).querySelector('.add-info-wrap')
          this.addInfoWrap.remove();
          this.showMoreBtn.classList.remove('hidden')
        })
      }
    
      showMoreInfo = () => {
        this.showMoreBtn = document.getElementById(`${this.options.id}`).querySelector('#showMore');
        this.showMoreBtn.addEventListener('click', e => {
          this.showMoreBtn.classList.add('hidden')
          const cardBody = document.getElementById(`${this.options.id}`).querySelector('.card-body');
          cardBody.insertAdjacentHTML('beforeend', `
            <div class="add-info-wrap">
            <p data-class="visitPurpose" class="card-purpose card-text">Visit purpose: ${this.options.visitPurpose}</p>
              <p data-class="lastVisit" class="card-last-visit card-text">Last visit: ${this.options.lastVisit}</p>
              <p data-class="visitDescription" class="card-description card-text">Description: ${this.options.visitDescription}</p>
              <button id="showLess" class="btn btn-primary">Show less</button>
            </div>
          `)
          this.showLessInfo();
        })
      }
  
    render() {
        super.render();
        this.showMoreInfo();
    }
} 

export default DentistCard;