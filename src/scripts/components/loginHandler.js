import SigninModal from "./Modal/SigninModal.js";
import login from "./login.js";
import { renderUserCards } from "./renderUserCards.js";
import { isDev } from "../constants/constants.js";




function loginHandler() {
    const root = document.querySelector("#root");
    const mail = "topsecret@mail.com";
    const pass = "qwerty";
    const signInElements = [...document.querySelectorAll('#singIn, .please-sign-in')];

    signInElements.forEach(elem => {
        elem.addEventListener("click", async (event) => {
            event.preventDefault();
            const authWindow = new SigninModal(login, renderUserCards);

            if (isDev) {
                authWindow.emailInput.value = mail;
                authWindow.passInput.value = pass;
            }

            authWindow.render(root);
        });
    })

}

export default loginHandler;