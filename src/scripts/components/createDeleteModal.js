import DeleteModal from "./Modal/DeleteModal.js";
import { deleteCard } from "../requests/requests.js";
import { usersData } from "../constants/constants.js";

export function createDeleteModal() {
  const removeHandler = async () => {
    const response = await deleteCard(this.options.id);
    if (response.ok) {
      const card = document.getElementById(`${this.options.id}`);
      card.remove();
      const indexRemovedCard = usersData.findIndex(
        (card) => card.id === this.options.id
      );
      usersData.splice(indexRemovedCard, 1);
    }
  };
  switch (this.options.doctor) {
    case "Cardiologist":
      return new DeleteModal("Визит к кардиологу", removeHandler).render();
    case "Therapist":
      return new DeleteModal("Визит к терапевту", removeHandler).render();
    default:
      return new DeleteModal("Визит к дантисту", removeHandler).render();
  }
}
