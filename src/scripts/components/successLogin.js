function successLogin(token, renderCardsFn) {
  const singInBtn = document.querySelector("#singIn");
  const createBtn = document.querySelector("#createVisit");
  const logoutBtn = document.querySelector("#logout");
  const signInTitle = document.querySelector(".please-sign-in");
  const filterWrap = document.querySelector(".filter__wrap");
  if (token) {
    singInBtn.classList.toggle("hidden");
    createBtn.classList.toggle("hidden");
    logoutBtn.classList.toggle("hidden");
    filterWrap.classList.remove("hidden");
    signInTitle.remove();
    if (renderCardsFn) {
      renderCardsFn();
    }
  }
}

export default successLogin;
