import Modal from "./Modal/Modal.js";
import defineDoctor from "./defineDoctor.js";
import { doctors, urgency } from "../constants/constants.js";
import { postCard } from "../requests/requests.js";
import CardiologistCard from "./Card/CardiologistCard.js";
import DentistCard from "./Card/DentistCard.js";
import TherapistCard from "./Card/TherapistCard.js";
import { createDeleteModal } from "./createDeleteModal.js";
import createEditModal from "./createEditModal.js";
import { usersData } from "../constants/constants.js";

function createVisitHandler() {
  const createBtn = document.querySelector("#createVisit");

  createBtn.addEventListener("click", (event) => {
    event.preventDefault();
    const root = document.body;
    const selectDoctor = document.createElement("select");
    selectDoctor.classList.add("form-select");
    const chooseYourDoctor = document.createElement("option");
    chooseYourDoctor.textContent = "Choose your doctor";
    selectDoctor.prepend(chooseYourDoctor);
    doctors.forEach((doctor) => {
      const option = document.createElement("option");
      option.value = doctor;
      option.textContent = doctor;
      selectDoctor.append(option);
    });
    const modal = new Modal();

    modal.renderVisit(root, selectDoctor);
    selectDoctor.addEventListener("change", (e) => {
      const selectedDoctor = e.target.value;
      modal.addContent(defineDoctor(selectedDoctor));
      const form = document.getElementById("formElem");
      form.onsubmit = async (e) => {
        e.preventDefault();
        const testData = new FormData(form);
        const formDataObject = {};
        formDataObject.doctor = selectedDoctor;
        formDataObject.status = 'open';
        for (const [key, value] of testData.entries()) {
          formDataObject[key] = value;
        }
        console.log(formDataObject);
        const result = await postCard(formDataObject);

        console.log(result);
        console.log(usersData);
        if (result) {
          usersData.push(result);
          console.log(usersData);
          modal.closeModal();
        }

        switch (selectedDoctor) {
          case "Cardiologist":
            return new CardiologistCard(result, createDeleteModal, createEditModal).render();
          case "Dentist":
            return new DentistCard(result, createDeleteModal, createEditModal).render();
          case "Therapist":
            return new TherapistCard(result, createDeleteModal, createEditModal).render();
        }
      };
    });
  });
}

export default createVisitHandler;
