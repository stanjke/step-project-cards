import { postCard } from "../requests/requests.js";

export async function createCard() {
    const response = await postCard();
}