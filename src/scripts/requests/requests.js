import { API } from "../constants/constants.js";
import { authRequest } from "../helpers/authRequest.js";
import { sendRequest } from "../helpers/sendRequest.js";

export async function getToken(email, password, login = "login") {
  const token = await authRequest(`${API}/${login}`, {
    body: JSON.stringify({ email: email, password: password }),
  });
  return token;
}

export async function getAllCards() {
  const cardsList = await sendRequest(API, localStorage.getItem("token"));
  return cardsList;
}

export async function postCard(data) {
  const response = await sendRequest(
    API,
    localStorage.getItem("token"),
    "POST",
    {
      body: JSON.stringify(data),
    }
  );
  return response;
}

export function deleteCard(cardId) {
  const response = sendRequest(
    `${API}/${cardId}`,
    localStorage.getItem("token"),
    "DELETE"
  );
  return response;
}

export async function editCard(cardId, data) {
  const response = await sendRequest(
    `${API}/${cardId}`,
    localStorage.getItem('token'),
    "PUT",
    {
      body: JSON.stringify(data)
    }
  )
  return response;
}
