import { editCard } from "../requests/requests.js";
import EditModal from "./Modal/EditModal.js";
import collectFormData from "./collectFormData.js";
import { usersData } from "../constants/constants.js";

function createEditModal() {
    const editHandler = async () => {
        const response = await editCard(this.options.id, collectFormData.call(this))
        this.edit(response);
        const card = document.getElementById(`${this.options.id}`);
        const indexEditedCard = usersData.findIndex(
            (card) => card.id === this.options.id
        );
        usersData.splice(indexEditedCard, 1, response);
    }
    new EditModal(this.options, editHandler.bind(this)).render();
}

export default createEditModal;
