import Card from "./Card.js";

class CardiologistCard extends Card {
    constructor(options, deleteFn, editFn) {
        super(options, deleteFn, editFn);
    }

    showLessInfo = () => {
        this.showLessBtn = document.getElementById(`${this.options.id}`).querySelector('#showLess');
        this.showLessBtn.addEventListener('click', () => {
          this.addInfoWrap = document.getElementById(`${this.options.id}`).querySelector('.add-info-wrap')
          this.addInfoWrap.remove();
          this.showMoreBtn.classList.remove('hidden')
        })
      }
    
      showMoreInfo = () => {
        this.showMoreBtn = document.getElementById(`${this.options.id}`).querySelector('#showMore');
        this.showMoreBtn.addEventListener('click', e => {
          this.showMoreBtn.classList.add('hidden')
          const cardBody = document.getElementById(`${this.options.id}`).querySelector('.card-body');
          cardBody.insertAdjacentHTML('beforeend', `
            <div class="add-info-wrap">
            <p data-class="visitPurpose" class="card-purpose card-text">Visit purpose: ${this.options.visitPurpose}</p>
              <p data-class="age" class="card-age card-text">Age: ${this.options.age}</p>
              <p data-class="BMI" class="card-bmi card-text">BMI: ${this.options.BMI}</p>
              <p data-class="pressure" class="card-pressure card-text">Pressure: ${this.options.pressure}</p>
              <p data-class="diseaseHistory" class="card-history card-text">History: ${this.options.diseaseHistory}</p>
              <p data-class="visitDescription" class="card-description card-text">Description: ${this.options.visitDescription}</p>
              <button id="showLess" class="btn btn-primary">Show less</button>
            </div>
          `)
          this.showLessInfo();
        })
      }
  
    render() {
        super.render();
        this.showMoreInfo();
    }
} 

export default CardiologistCard;