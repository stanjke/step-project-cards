import Modal from "./Modal.js";

class DeleteModal extends Modal {
    constructor(title, submitFn) {
        super();
        this.title = title;
        this.submitFn = submitFn;
        this.deleteTitle = document.createElement('h3');
    }

    createEl() {
        super.createEl();
        this.deleteTitle.textContent = `Are you sure you want to delete ${this.title}?`;
        this.confirmBtn.classList.add('modal__confirm-btn');
        this.cancelBtn.classList.add('modal__cancel-btn');
        this.confirmBtn.textContent = 'Confirm';
        this.cancelBtn.textContent = 'Cancel'
        this.confirmBtn.addEventListener('click', () => {
            this.submitFn();
            this.closeModal();
        })
        this.cancelBtn.addEventListener('click', this.closeModal.bind(this));
        this.modalContentWrap.append(this.deleteTitle);
        this.modalButtonWrap.append(this.confirmBtn, this.cancelBtn);
    }


}

export default DeleteModal;