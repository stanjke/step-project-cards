function logout() {
    localStorage.clear();
    location.reload(true);
}

export default logout;