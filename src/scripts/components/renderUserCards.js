import { getAllCards } from "../requests/requests.js";
import { createDeleteModal } from "./createDeleteModal.js";
import createEditModal from "./createEditModal.js";
import CardiologistCard from "./Card/CardiologistCard.js";
import { usersData } from "../constants/constants.js";
import DentistCard from "./Card/DentistCard.js";
import TherapistCard from "./Card/TherapistCard.js";
export async function renderUserCards() {
  usersData.push(...(await getAllCards()));

  if (usersData.length === 0) {

  } else {
    usersData.forEach((visit) => {
      switch (visit.doctor) {
        case 'Cardiologist':
          return new CardiologistCard(visit, createDeleteModal, createEditModal).render();
        case 'Therapist':
          return new TherapistCard(visit, createDeleteModal, createEditModal).render();
        default:
          return new DentistCard(visit, createDeleteModal, createEditModal).render();
      }
    });
  }

  // if(cards.length <= 0) {
  //     console.log("Array of cards is empty");

  // }
}
