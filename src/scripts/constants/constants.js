export const API = "https://ajax.test-danit.com/api/v2/cards";
export const doctors = ["Cardiologist", "Dentist", "Therapist"];
export const urgency = ["routine", "high-priority", "urgent"];
export const isDev = true;
export const usersData = [];
export const CARD_CLASSES = ['.card-urgency, .card-doctor, .card-purpose, .card-fullname, .card-description, .card-age, .card-last-visit, .card-bmi, .card-pressure, .card-history']

