import Modal from "../Modal/Modal.js";

class Visit extends Modal {
  constructor(defineFn) {
    super();
    this.defineFn = defineFn;
  }

  createEl() {
    super.createEl();
    this.modalContainer.classList.add("modal__main-container--small");
  }

}

export default Visit;