

class Modal {
  constructor() {
    this.modalElement = document.createElement("div");
    this.modalBackground = document.createElement("div");
    this.modalContainer = document.createElement("div");
    this.modalCloseButton = document.createElement("button");
    this.modalContentWrap = document.createElement("div");
    this.modalButtonWrap = document.createElement("div");
    this.confirmBtn = document.createElement('button');
    this.cancelBtn = document.createElement('button');
  }

  createEl() {
    this.modalElement.classList.add("modal");
    this.modalBackground.classList.add("modal__background");
    this.modalContainer.classList.add("modal__main-container");
    this.modalCloseButton.classList.add("modal__close");
    this.modalContentWrap.classList.add("modal__content-wrapper");
    this.modalButtonWrap.classList.add("modal__button-wrapper");
    this.modalContainer.append(
      this.modalCloseButton,
      this.modalContentWrap,
      this.modalButtonWrap
    );
    this.modalElement.append(this.modalBackground, this.modalContainer);
    this.modalCloseButton.addEventListener("click", this.closeModal.bind(this));
    this.modalBackground.addEventListener("click", this.closeModal.bind(this));
  }

  closeModal() {
    this.modalElement.remove();
  }

  render(parent = document.body) {
    this.createEl();
    parent.append(this.modalElement);
  }

  addContent(addElem) {
    this.modalButtonWrap.textContent = "";
    this.modalButtonWrap.append(addElem);
  }

  renderVisit(parent, content) {
    this.createEl();
    this.modalContentWrap.append(content);
    parent.append(this.modalElement);
  }
}

export default Modal;
