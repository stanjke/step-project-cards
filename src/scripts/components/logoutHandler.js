import logout from "./logout.js";

function logoutHandler() {
    const logoutBtn = document.querySelector('#logout');
    logoutBtn.addEventListener('click', logout);
}

export default logoutHandler;