import { usersData } from "../constants/constants.js";
import { createDeleteModal } from "./createDeleteModal.js";
import createEditModal from "./createEditModal.js";
import DentistCard from "./Card/DentistCard.js";
import CardiologistCard from "./Card/CardiologistCard.js";
import TherapistCard from "./Card/TherapistCard.js";

function filter() {
  const cardsWrap = document.querySelector(".card__wrap");
  const filterBtn = document.querySelector("#filter");
  const filterForm = document.querySelector("#filterForm");
  const closeFilterBtn = document.querySelector("#closeFilterBtn");
  filterBtn.addEventListener("click", (e) => {
    e.preventDefault();
    const filterFormObj = {};
    const filterData = new FormData(filterForm);
    closeFilterBtn.classList.remove("hidden");

    for (const [key, value] of filterData.entries()) {
      filterFormObj[key] = value;
    }

    const filteredCards = usersData.filter((user) => {
      let match = true;

      if (filterFormObj.fullName && user.fullName !== filterFormObj.fullName) {
        match = false;
      }

      if (filterFormObj.urgency && user.urgency !== filterFormObj.urgency) {
        match = false;
      }

      if (
        (filterFormObj.status
          ? filterFormObj.status.toLowerCase()
          : filterFormObj.status) &&
        user.status !== filterFormObj.status.toLowerCase()
      ) {
        match = false;
      }

      return match;
    });

    cardsWrap.innerHTML = "";

    if (filteredCards.length === 0) {
    } else {
      filteredCards.forEach((visit) => {
        switch (visit.doctor) {
          case "Cardiologist":
            return new CardiologistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
          case "Therapist":
            return new TherapistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
          default:
            return new DentistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
        }
      });
    }
    closeFilterBtn.addEventListener("click", (e) => {
      cardsWrap.innerHTML = "";
      usersData.forEach((visit) => {
        switch (visit.doctor) {
          case "Cardiologist":
            return new CardiologistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
          case "Therapist":
            return new TherapistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
          default:
            return new DentistCard(
              visit,
              createDeleteModal,
              createEditModal
            ).render();
        }
      });
      closeFilterBtn.classList.add("hidden");
    });
  });
}

export default filter;
