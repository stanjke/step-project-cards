export async function authRequest(url, options) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        ...options
    })

    return {
        token: await response.text(),
        isSuccess: response.ok
    }
}