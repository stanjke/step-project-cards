import { CARD_CLASSES } from "../../constants/constants.js";

class Card {
  constructor(options, deleteFn, editFn) {
    this.editFn = editFn;
    this.deleteFn = deleteFn;
    this.options = options;
    this.cardWrap = document.querySelector(".card__wrap");
  }

  edit(object) {
    this.card = document.getElementById(`${this.options.id}`);
    for (let key in object) {
      if (this.options.hasOwnProperty(key)) {
        this.options[key] = object[key]
      }
    }

    this.cardElements = [...this.card.querySelectorAll(CARD_CLASSES)];
    this.cardElements.forEach(element => {
      let dataClass = element.getAttribute('data-class')
      if (dataClass !== null) {
        if (this.options.hasOwnProperty(dataClass)) {
          let parts = element.textContent.split(":");
          if (parts.length > 1) {
            parts[1] = ` ${this.options[dataClass]}`;
            element.textContent = parts.join(':');
          } else {
            element.textContent = `${this.options[dataClass]}`
          }
        }
      }

    })
  }


  setDeleteBtn = () => {
    this.deleteBtn = document
      .getElementById(`${this.options.id}`)
      .querySelector(".card__close");
    this.deleteBtn.addEventListener("click", this.deleteFn.bind(this));
  };

  setEditBtn = () => {
    this.editBtn = document
      .getElementById(`${this.options.id}`)
      .querySelector(".card__edit");
    this.editBtn.addEventListener("click", this.editFn.bind(this));
  };

  defineDoctor = () => {
    switch (this.options.doctor) {
      case "Cardiologist":
        return "Визит к кардиологу";
      case "Therapist":
        return "Визит к терапевту";
      default:
        return "Визит к дантисту";
    }
  };

  render() {
    this.cardWrap.insertAdjacentHTML('beforeend', `
      <div class="card" id="${this.options.id}">
        <div class="card-header d-flex justify-content-between align-items-center">
         <p class="card-urgency mb-0" data-class="urgency">${this.options.urgency}</p>
         <div class="btn-wrap d-flex align-items-center gap-1">
          <button class="card__edit"></button>
          <button class="card__close"></button>
         </div>
        </div>
        <div class="card-body">
          <h5 class="card-doctor card-title">${this.defineDoctor()}</h5>
          <p data-class="fullName" class="card-fullname card-text">${this.options.fullName}</p>
          <button id="showMore" class="btn btn-primary">Show more</button>
        </div>
        <div class="card-footer text-body-secondary">
          2 days ago
        </div>
      </div>
    `
    );
    this.setDeleteBtn();
    this.setEditBtn();
  }
}

export default Card;
